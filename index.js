const http = require('http');

const port = 3000;

http.createServer((req, res) => {
	if(req.url == '/login') {
		res.writeHead(200, {'Content-Type': 'text/plan'});
		res.end('Welcome to the login page.');
	} else {
		res.writeHead(200, {'Content-Type': 'text/plan'});
		res.end('I\'m sorry the page you are looking for cannot be found.');
	}
}).listen(port);

console.log(`Server now accessible at localhost:${port}`);